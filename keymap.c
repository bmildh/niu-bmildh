#include QMK_KEYBOARD_H

/* TAP DANCE */
enum {
  TD_SLSH_BSLS = 0 /* this will tap between slash and backslash */
};

/* BMILDH LAYERS */
enum {
  BASE = 0,
  SYMB,
  NUMS,
  FKEY,
  MISC
};

qk_tap_dance_action_t tap_dance_actions[] = {
	[TD_SLSH_BSLS] = ACTION_TAP_DANCE_DOUBLE(KC_SLSH, KC_BSLS)
};


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

	/* Layer 0, BASE LAYER
	 * ,-----------------------------------------------------------------------------------.
	 * | Esc  |   Q  |   W  |   E  |   R  |   T  |   Y  |   U  |   I  |   O  |   P  | Bksp |
	 * |------+------+------+------+------+-------------+------+------+------+------+------|
	 * | Tab  |   A  |   S  |   D  |   F  |   G  |   H  |   J  |   K  |   L  |   ;  |  "   |
	 * |------+------+------+------+------+------|------+------+------+------+------+------|
	 * | Shift|   Z  |   X  |   C  |   V  |   B  |   N  |   M  |   ,  |   .  |  Up  |Enter |
	 * |------+------+------+------+------+------+------+------+------+------+------+------|
	 * | Caps | LCTL | LWIN |  Alt |Layer1|    Space    |Layer2| Alt  |  =   |  /\  |Ctrl  |
	 * `-----------------------------------------------------------------------------------'
	 */
	[0] = LAYOUT_planck_mit(
		KC_ESC,  KC_Q,    KC_W,   KC_E,    KC_R,  KC_T,   KC_Y,   KC_U,  KC_I,    KC_O,    KC_P,    KC_BSPC,
		KC_TAB,  KC_A,    KC_S,   KC_D,    KC_F,  KC_G,   KC_H,   KC_J,  KC_K,    KC_L,    KC_SCLN, KC_ENT,
		KC_LSFT, KC_Z,    KC_X,   KC_C,    KC_V,  KC_B,   KC_N,   KC_M,  KC_COMM, KC_DOT,  KC_RSFT, KC_QUOTE,
		KC_CAPS, KC_LCTL, KC_LWIN,  KC_LALT, MO(SYMB),     KC_SPC,     TO(NUMS), KC_RCTL, KC_RALT,  TD(TD_SLSH_BSLS), KC_RCTL 
  ),

	/* Layer 1, SYMBOL LAYER
	 * ,-----------------------------------------------------------------------------------.
	 * |   `  |      | DEL  | END  | HOME |   $  |  =   |  (   |  )   |  [   |   ]  |  -   |
	 * |------+------+------+------+------+-------------+------+------+------+------+------|
	 * |      |      |      | PGDN | PGUP |   &  |  !   |  {   |  }   |  <   |   >  |  _   |
	 * |------+------+------+------+------+------|------+------+------+------+------+------|
	 * |      |      |      |      |      |   %  |  ~   |  *   |  #   |  ?   |  UP  |      |
	 * |------+------+------+------+------+------+------+------+------+------+------+------|
	 * | Reset|      |      |      |      |    Space    |   @  |  |   | LEFT | DOWN |RIGHT |
	 * `-----------------------------------------------------------------------------------'
	 */
	[1] = LAYOUT_planck_mit(
		KC_NO, KC_NO, KC_DEL, KC_END,  KC_HOME, KC_DLR,  KC_EQL,  KC_LPRN, KC_RPRN, KC_LBRC, KC_RBRC, KC_PMNS,
		KC_NO, KC_NO, KC_NO,  KC_PGDN, KC_PGUP, KC_AMPR, KC_EXLM, KC_LCBR, KC_RCBR, KC_LT,   KC_GT,   KC_UNDS,
		KC_NO, KC_NO, KC_NO,  KC_NO,   KC_NO,   KC_PERC, KC_TILD, KC_ASTR, KC_HASH, KC_QUES, KC_UP,   KC_NO,
		RESET, KC_NO, KC_NO,  KC_NO,   KC_TRNS,     KC_TRNS,        KC_AT,   KC_PIPE, KC_LEFT, KC_DOWN, KC_RIGHT
	),

	/* Layer 2, NUMBER LAYER
	 * ,-----------------------------------------------------------------------------------.
	 * |L-BASE|   1  |   2  |   3  |   4  |   5  |   6  |   7  |   8  |   9  |   0  |BCKSC |
	 * |------+------+------+------+------+-------------+------+------+------+------+------|
	 * |      |      |      |      |      |      |      |   -  |  +   |  *   |  /   |Enter |
	 * |------+------+------+------+------+------|------+------+------+------+------+------|
	 * |      |      |      |      |      |      |      |      |      |      |  UP  |      |
	 * |------+------+------+------+------+------+------+------+------+------+------+------|
	 * |      |      |      |      |Layer1|    Space    |      |      | LEFT | DOWN |RIGHT |
	 * `-----------------------------------------------------------------------------------'
	 */
	[2] = LAYOUT_planck_mit(
    TO(BASE), KC_1,  KC_2,  KC_3,  KC_4,  KC_5,  KC_6,  KC_7,     KC_8,    KC_9,    KC_0,    KC_BSPC,
		KC_NO,    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_PMNS,  KC_PLUS, KC_PAST, KC_PSLS, KC_ENT,
		KC_NO,    KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,    KC_NO,   KC_NO,   KC_UP,   KC_NO,
		KC_NO,    KC_NO, KC_NO, KC_NO, KC_NO,    KC_TRNS,   TO(FKEY), KC_NO,   KC_LEFT, KC_DOWN, KC_RIGHT
  ),


  /* Layer 3, FUNCTION LAYER
	 * ,-----------------------------------------------------------------------------------.
	 * |L-BASE|  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |  F7  |  F8  |  F9  | F10  |      |
	 * |------+------+------+------+------+-------------+------+------+------+------+------|
	 * |      |      |      |      |      |      |      |  F11 |  F12 |      |      |
	 * |------+------+------+------+------+------|------+------+------+------+------+------|
	 * |      |      |      |      |      |      |      |      |      |      |      |      |
	 * |------+------+------+------+------+------+------+------+------+------+------+------|
	 * |      |      |      |      |      |    Space    |      |      |      |      |      |
	 * `-----------------------------------------------------------------------------------'
	 */
	[3] = LAYOUT_planck_mit(
    TO(BASE), KC_F1,  KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7,  KC_F8,  KC_F9, KC_F10, KC_NO,
		KC_NO,    KC_NO,  KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_F11, KC_F12, KC_NO, KC_NO, KC_NO,
		KC_NO,    KC_NO,  KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO,  KC_NO,  KC_NO, KC_NO, KC_NO,
		KC_NO,    KC_NO,  KC_NO, KC_NO, KC_NO,    KC_TRNS,   KC_NO,  KC_NO,  KC_NO, KC_NO, KC_NO
  )
}; 


void matrix_init_user(void) {
}

void matrix_scan_user(void) {
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	return true;
}

void led_set_user(uint8_t usb_led) {

	if (usb_led & (1 << USB_LED_NUM_LOCK)) {

	} else {

	}

	if (usb_led & (1 << USB_LED_CAPS_LOCK)) {

	} else {

	}

	if (usb_led & (1 << USB_LED_SCROLL_LOCK)) {

	} else {

	}

	if (usb_led & (1 << USB_LED_COMPOSE)) {

	} else {

	}

	if (usb_led & (1 << USB_LED_KANA)) {

	} else {

	}

}
