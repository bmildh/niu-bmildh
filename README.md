# Niu-bmildh

My personal keymapping for the Niu mini keyboard.

This keyboard is growing on me every day. It is now my daily driver. 
Ortholinear layout, looks very much like a planck keyboard. Brown switches.

[QMK](https://qmk.fm/) firmware is awesome. I will never again use a keyboard that I cannot flash. 

The microcontroller is the atmega32u4. 


Ordered pcb from [kbdfans](https://kbdfans.com/products/niu-mini-40-diy-kit)

# On linux

- You need the qmk cli from the AUR. 
- git clone the qmk-firmware
- Install dfu-programmer

# Basic instructions:

Download the qmk firmware. Build your own version. 

## My workflow

```
cd qmk_firmware/keyboards/niu_mini/keymaps/
git clone git@gitlab.com:bmildh/niu-bmildh.git
```
Edit the keymap.c file. 

Go back to qmk root dir. Issue commands:
```
qmk compile -kb niu_mini -km niu-bmildh
```

Note(bmildh): you need to sudo - otherwise you will get an error message:

> dfu-programmer: no device present


1. Reset the keyboard (underneath, there is a pinhole reset button)
2. sudo dfu-programmer atmega32u4 erase
3. sudo dfu-programmer atmega32u4 flash .build/niu_mini_bmildh.hex
4. sudo dfu-programmer atmega32u4 reset

# Miscellaneous

## Enable tap dance

In the file:
> qmk_firmware/keyboards/rules.mk

```
#define TAP_DANCE_ENABLE yes
```

## Switching layer with momentarily switch

When you use the MO(to-layer) switch, make sure that the corresponding key in
the momentarily layer is bound to KC_TRNS and NOT to KC_NO. That is, the key
that is used to momentarily switch layer, cannot be used in that layer since it
is being hold down. Makes sense?




